#include <iostream>
#include <string>
#include <algorithm>
#include <cstdio>
#include <fstream>

#define NEW_CONTACT_ID_BEFORE_ADD -10293
#define NUMBER_OF_CONTACT_DATA 6
#define CONTACT_LIST_FILE_ADDRESS "contacts.csv"

using namespace std;

enum Return_values
{
	cmd_FAILED = -1,
	cmd_OK = 0,
	cmd_NON = 2,
};

enum Phone_number_validation_values 
{
	PHONE_NUMBER_SIZE = 11,
	FIRST_DIGIT_IN_VALID_PHONE_NUMBER = '0',
	SECOND_DIGIT_IN_VALID_PHONE_NUMBER = '9'
};

enum Command_sizes
{
	ADD_COMMAND_SIZE = 3,
	NON_ADD_COMMAND_SIZE = 6
};

enum Commands
{
	_Add = 'a',
	_Update = 'u',
	_Erase = 'e',
	_Search = 's'
};

enum Flags
{
	_First_name = 'f',
	_Last_name = 'l',
	_Phone_numer = 'p',
	_Email_address = 'e',
	_Address = 'a'
};

enum Flags_csv_idexes
{
	_First_name_flag_csv_index = 1,
	_Last_name_flag_csv_index,
	_Email_address_flag_csv_index,
	_Phone_numer_flag_csv_index,
	_Address_flag_csv_index
};

struct Contact
{
	int ID;
	string first_name;
	string last_name;
	string phone_number;
	string email_address;
	string address;
	Contact* next_contact;
};

void COMMAND_FAILED()
{
	cout << "Command Failed" << endl;
}

void COMMAND_OK()
{
	cout << "Command Ok" << endl;
}

void print_contact (Contact* contact)
{
	cout << contact->ID << " " << contact->first_name << " " << contact->last_name << " ";
	cout << contact->email_address << " " << contact->phone_number << " ";
	cout << contact->address << endl;
}

bool check_phoneNumber (string pnum)
{
	for (int i = 0; i < (int)pnum.size(); i++)
		if(pnum[i] < '0' || pnum[i] > '9')
			return 0;
	return (pnum.size() == PHONE_NUMBER_SIZE && pnum[0] == FIRST_DIGIT_IN_VALID_PHONE_NUMBER && pnum[1] == SECOND_DIGIT_IN_VALID_PHONE_NUMBER);
}

bool check_emailAddress (string email)
{
	int at_flag = -1, dot_flag = -1;
	for (int i = 0; i < (int)email.size(); i++)
	{
		if(email[i] == '@')
		{
			if(at_flag != -1)
				return 0;
			at_flag = i;
		}
		else if(email[i] == '.')
			dot_flag = i;
		else if(email[i] == ',')
			return 0;
	}
	if(dot_flag == at_flag + 1)
		return 0;
	return (at_flag != -1 && dot_flag != -1 && dot_flag > at_flag);
}

bool check_being_unique (Contact* new_contact, Contact* last_contact, int ID)
{
	while(last_contact != NULL)
	{
		if(last_contact->ID == ID)
		{
			last_contact = last_contact->next_contact;
			continue;
		}
		if(new_contact->email_address == last_contact->email_address)
			return 1;
		if(new_contact->phone_number == last_contact->phone_number)
			return 1;
		if(new_contact->first_name == last_contact->first_name &&
			new_contact->last_name == last_contact->last_name)
			return 1;
		last_contact = last_contact->next_contact;
	}
	return 0;
}

void erase_extra_whiteSpaces (string &command)
{
	string final_command = "";
	bool Is_extra = 0;
	for (int i = 0; i < (int)command.size(); i++)
	{
		if((command[i] == ' ' || command[i] == '\t') && Is_extra)
			continue;
		else
		{
			if(command[i] == '\t')
				command[i] = ' ';
			final_command += command[i];
			Is_extra = 0;
		}
		if(!Is_extra && command[i] == ' ')
			Is_extra = 1;
	}
	command = final_command;
}

int extract_contact_in_command(string command, Contact* contact)
{
	string tmp = "";
	int ind = command.size();
	int cmd_size_checker = 0;
	for (int i = 0; i < (int)command.size() - 1; i++)
	{
		if(command[i] == '-' && command[i - 1] == ' ')
		{
			tmp = "";
			for (ind = i + 3; ind < (int)command.size(); ind++)
				if(command[ind] == ' ' && (ind == (int)command.size() - 1 || command[ind + 1] == '-'))
					break;
				else if(command[ind] == ' ' && command[i + 1] != _Address && (ind != (int)command.size() - 1) && command[ind + 1] != '-')
					return cmd_FAILED;
				else tmp += command[ind];

			if(tmp[0] == '-')
				return cmd_FAILED;
			cmd_size_checker += (int)tmp.size() + 4;

			if(command[i + 1] == _Last_name && (int)tmp.size())
				contact->last_name = tmp;
			else if(command[i + 1] == _First_name && (int)tmp.size())
				contact->first_name = tmp;
			else if(command[i + 1] == _Phone_numer && check_phoneNumber(tmp) && (int)tmp.size())
				contact->phone_number = tmp;
			else if(command[i + 1] == _Email_address && check_emailAddress(tmp) && (int)tmp.size())
				contact->email_address = tmp;
			else if(command[i + 1] == _Address && (int)tmp.size())
				contact->address = tmp;
			else
				return cmd_FAILED;
			i = ind;
		}
	}
	if(command[0] == _Add)
		cmd_size_checker += ADD_COMMAND_SIZE;
	else cmd_size_checker += NON_ADD_COMMAND_SIZE;
	if(command[0] != _Update && cmd_size_checker != (int)command.size())
		return cmd_FAILED;
	return cmd_size_checker;
}

int max_ID_in_contact_list (Contact* last_contact)
{
	Contact* cur_contact = last_contact;
	int max_ID = -1;
	while(cur_contact != NULL)
	{
		max_ID = max(max_ID, cur_contact->ID);
		cur_contact = cur_contact->next_contact;
	}
	return max_ID;
}

bool check_add_command (Contact* new_contact, Contact* last_contact)
{
	if(new_contact->first_name == "")
		return 0;
	if(new_contact->last_name == "")
		return 0;
	if(new_contact->phone_number == "")
		return 0;
	if(new_contact->email_address == "")
		return 0;
	if(check_being_unique(new_contact, last_contact, -1))
		return 0;
	return 1;
}

int add_contact (Contact* new_contact, Contact* &last_contact)
{
	if(!check_add_command(new_contact, last_contact))
		return -1;
	if(new_contact->ID == NEW_CONTACT_ID_BEFORE_ADD)
		new_contact->ID = max_ID_in_contact_list(last_contact) + 1;
	new_contact->next_contact = last_contact;
	last_contact = new_contact;
	return cmd_OK;
}

bool check_existence_of_keyword_in_str(string keyword, string str)
{
	bool noteq_flag = 0;
	for (int i = 0; i <= (int)str.size() - (int)keyword.size(); i++)
	{
		noteq_flag = 0;
		for (int j = 0; j < (int)keyword.size(); j++)
			if(str[i + j] != keyword[j])
			{
				noteq_flag = 1;
				break;
			}
		if(!noteq_flag)
			return 1;
	}
	return 0;
}

int search_contacts (string keyword, Contact* last_contact)
{
	bool print_flag = 0;
	while(last_contact != NULL)
	{
		print_flag = 0;
		if( check_existence_of_keyword_in_str(keyword, last_contact->first_name) )
			print_flag = 1;
		else if( check_existence_of_keyword_in_str(keyword, last_contact->last_name) )
			print_flag = 1;
		else if( check_existence_of_keyword_in_str(keyword, last_contact->phone_number) )
			print_flag = 1;
		else if( check_existence_of_keyword_in_str(keyword, last_contact->email_address) )
			print_flag = 1;
		else if( check_existence_of_keyword_in_str(keyword, last_contact->address) )
			print_flag = 1;
		if(print_flag)
			print_contact(last_contact);
		last_contact = last_contact->next_contact;
	}
	return cmd_OK;
}

int delete_contact (int ID, Contact* &last_contact)
{
	Contact* prv = NULL;
	Contact* cur = last_contact;
	bool founded_and_deleted = 0;
	while(cur != NULL)
	{
		if(cur->ID == ID)
		{
			if(cur == last_contact)
				last_contact = last_contact->next_contact;
			else
				prv->next_contact = cur->next_contact;
			delete cur;
			founded_and_deleted = 1;
			break;
		}
		prv = cur;
		cur = cur->next_contact;
	}
	if(!founded_and_deleted)
		return cmd_FAILED;
	return cmd_OK;
}

int update_contact (int ID, Contact* upd_contact, Contact* &last_contact)
{
	Contact* cur = last_contact;
	bool founded_and_updated = 0;
	while(cur != NULL)
	{
		if(cur->ID == ID)
		{
			if(upd_contact->first_name == "")
				upd_contact->first_name = cur->first_name;
			if(upd_contact->last_name == "")
				upd_contact->last_name = cur->last_name;
			if(upd_contact->email_address == "")
				upd_contact->email_address = cur->email_address;
			if(upd_contact->phone_number == "")
				upd_contact->phone_number = cur->phone_number;
			if(upd_contact->address == "")
				upd_contact->address = cur->address;

			if(!check_being_unique(upd_contact, last_contact, ID))
			{
				cur->first_name    = upd_contact->first_name   ;
				cur->last_name     = upd_contact->last_name    ;
				cur->phone_number  = upd_contact->phone_number ;
				cur->email_address = upd_contact->email_address;
				cur->address       = upd_contact->address      ;
				founded_and_updated = 1;
			}
			break;
		}
		cur = cur->next_contact;
	}
	if(!founded_and_updated)
		return cmd_FAILED;
	return cmd_OK;
}

int its_add_command (string command, Contact* &last_contact)
{
	Contact* new_contact = new Contact;
	if(extract_contact_in_command (command, new_contact) != -1)
	{
		new_contact->ID = NEW_CONTACT_ID_BEFORE_ADD;
		return add_contact(new_contact, last_contact);
	}
	else return cmd_FAILED;
}

int its_update_command(string command, Contact* &last_contact)
{
	int ID = 0, cmd_size_checker = 0;
	bool ID_exist = 0;
	for (int i = 7; i < (int)command.size(); i++)
	{
		if(command[i] == ' ')
		{
			for (int j = 7; j < i; j++)
				command[i] = ' ';
			erase_extra_whiteSpaces(command);
			cmd_size_checker++;
			break;
		}
		if('0' <= command[i] && command[i] <= '9')
		{
			ID = (ID * 10) + (command[i] - '0');
			cmd_size_checker++;
			ID_exist = 1;
		}
		else return cmd_FAILED;
	}
	if(!ID_exist)
		return cmd_FAILED;
	Contact* upd_contact = new Contact;
	int ext_cont_in_cmd = extract_contact_in_command(command, upd_contact);
	cmd_size_checker += ext_cont_in_cmd;
	if(ext_cont_in_cmd != -1 && cmd_size_checker == (int)command.size())
		return update_contact(ID, upd_contact, last_contact);
	else return cmd_FAILED;
}

int its_delete_command(string command, Contact* &last_contact)
{
	int ID = 0;
	bool ID_exist = 0;
	for (int i = 7; i < (int)command.size(); i++)
		if('0' <= command[i] && command[i] <= '9')
		{
			ID = (ID * 10) + (command[i] - '0');
			ID_exist = 1;
		}
		else return cmd_FAILED;
	if(ID_exist)
		return delete_contact(ID, last_contact);
	else return cmd_FAILED;
}

int command_analysis (string command, Contact* &last_contact)
{
	erase_extra_whiteSpaces(command);
	string tmp = "";
	for (int i = 0; i < 7; i++)
	{
		tmp += command[i];
		if(i == 3 && tmp == "add ")
			return its_add_command(command, last_contact);
	}
	if(tmp == "search ")
	{
		string keyword = "";
		for (int i = 7; i < (int)command.size(); i++)
			keyword += command[i];
		search_contacts(keyword, last_contact);
		return cmd_NON;
	}
	else if(tmp == "update ")
		return its_update_command(command, last_contact);
	else if(tmp == "delete ")
		return its_delete_command(command, last_contact);
	return cmd_FAILED;
}

int read_contacts(Contact* &last_contact)
{
	ifstream fin;
	fin.open(CONTACT_LIST_FILE_ADDRESS);
	if(fin == NULL)
		return -1;
	string contact_data, command;
	string extracted_contact_data[NUMBER_OF_CONTACT_DATA] = {"", "", "", "", "", ""};
	fin >> contact_data;
	int cnt = 0, new_ID = 0;
	while(!fin.eof())
	{
		getline(fin, contact_data);
		cnt = 0;
		new_ID = 0;
		for (int i = 0; i < NUMBER_OF_CONTACT_DATA; i++)
			extracted_contact_data[i] = "";
		for (int i = 0; i< (int)contact_data.size(); i++)
			if(contact_data[i] != ',' || cnt == _Address_flag_csv_index)
				extracted_contact_data[cnt] += contact_data[i];
			else
				cnt++;
		Contact* new_contact = new Contact;
		command = "add -f " + extracted_contact_data[_First_name_flag_csv_index] +
				  " -l " + extracted_contact_data[_Last_name_flag_csv_index] +
				  " -e " + extracted_contact_data[_Email_address_flag_csv_index] +
				  " -p " + extracted_contact_data[_Phone_numer_flag_csv_index];
		if(extracted_contact_data[_Address_flag_csv_index] != "")
			command += " -a " + extracted_contact_data[_Address_flag_csv_index];
		erase_extra_whiteSpaces(command);
		extract_contact_in_command(command, new_contact);
		for (int i = 0; i < (int)extracted_contact_data[0].size(); i++)
			new_ID = (new_ID * 10) + extracted_contact_data[0][i] - '0';
		new_contact->ID = new_ID;
		add_contact(new_contact, last_contact);
	}
	fin.close();

	return 0;
}

void save_contacts(Contact* last_contact)
{
	ofstream fout;
	fout.open(CONTACT_LIST_FILE_ADDRESS);
	Contact* tmp;
	fout << "ID,FIRSTNAME,LASTNAME,Email_Address,Phone_Number,Address\n";
	while(last_contact != NULL)
	{
		fout << last_contact->ID            << ",";
		fout << last_contact->first_name    << ",";
		fout << last_contact->last_name     << ",";
		fout << last_contact->email_address << ",";
		fout << last_contact->phone_number  << ",";
		fout << last_contact->address       <<endl;
		tmp = last_contact;
		last_contact = last_contact->next_contact;
		delete tmp;
	}
	fout.close();
}

int main()
{
	Contact* last_contact = NULL;
	string command;
	int execution_result = 0;

	read_contacts(last_contact);

	while(getline(cin, command))
	{
		if(command == "")
			continue;
		execution_result = command_analysis(command, last_contact);
		if(execution_result == cmd_FAILED)
			COMMAND_FAILED();
		else if(execution_result == cmd_OK)
			COMMAND_OK();
	}
	save_contacts(last_contact);
	return 0;
}
